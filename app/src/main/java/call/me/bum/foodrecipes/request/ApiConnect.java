package call.me.bum.foodrecipes.request;

import android.app.Service;
import call.me.bum.foodrecipes.request.service.RecipeApi;
import call.me.bum.foodrecipes.util.Constants;
import retrofit2.Retrofit;

public class ApiConnect {
  private static Retrofit retrofit = ServiceGenerator.getRetrofit(Constants.BASE_URL);
  public static RecipeApi getRecipeApi() {
    return retrofit.create(RecipeApi.class);
  }
}
