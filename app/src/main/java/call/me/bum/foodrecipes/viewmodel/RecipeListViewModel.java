package call.me.bum.foodrecipes.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import call.me.bum.foodrecipes.model.Recipe;
import call.me.bum.foodrecipes.repository.RecipeRepository;
import java.util.List;

// Solve what? What this ViewModel do
// Answer it retrieving, holding, displaying the recipe list display in activity
// Another viewmodel responsible for update it a list of the recipe
public class RecipeListViewModel extends ViewModel {

  private static RecipeRepository mRepository;
  private boolean mIsViewingRecipe;
  private boolean mIsPerformingQuery;

  public RecipeListViewModel() {
    mRepository = RecipeRepository.getInstance();
    mIsPerformingQuery = false;
  }

  public LiveData<List<Recipe>> getRecipes() {
    return mRepository.getRecipes();
  }

  public void searchRecipes(String query, int pageNumber) {
    mIsViewingRecipe = true;
    mIsPerformingQuery = true;
    mRepository.searchRecipes(query, pageNumber);
  }

  public static RecipeRepository getmRepository() {
    return mRepository;
  }

  public static void setmRepository(RecipeRepository mRepository) {
    RecipeListViewModel.mRepository = mRepository;
  }

  public boolean isViewingRecipe() {
    return mIsViewingRecipe;
  }

  public void setIsViewingRecipe(boolean isViewingRecipe) {
    this.mIsViewingRecipe = isViewingRecipe;
  }

  public boolean isPerformingQuery() {
    return mIsPerformingQuery;
  }

  public void setIsPerformingQuery(boolean isPerformingQuery) {
    this.mIsPerformingQuery = isPerformingQuery;
  }

  public boolean onBackPressed() {
    if (mIsViewingRecipe) {
      //cancel the query
      mRepository.cancelRequest();
      mIsPerformingQuery = false;
    }
    if (mIsViewingRecipe) {
      mIsViewingRecipe = false;
      return false;
    }
    return true;
  }

  public void searchNextPage() {
    if (!mIsPerformingQuery && mIsViewingRecipe) {
      mRepository.searchNextPage();
    }
  }
}
