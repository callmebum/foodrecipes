package call.me.bum.foodrecipes.request;

import call.me.bum.foodrecipes.util.Constants;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

  private static Retrofit retrofit = null;
  public static Retrofit getRetrofit(String baseUrl) {
    // Config Http request
    OkHttpClient.Builder builderClient = new OkHttpClient.Builder()
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .connectTimeout(30,TimeUnit.SECONDS)
        .retryOnConnectionFailure(true);

    retrofit = new Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    return retrofit;
  }

}
