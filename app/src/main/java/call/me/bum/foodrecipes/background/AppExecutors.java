package call.me.bum.foodrecipes.background;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

// Use to run Retrofit in background
public class AppExecutors {

  // Singleton
  private static AppExecutors instance;
  public static AppExecutors getInstance() {
    if (instance == null) {
      instance = new AppExecutors();
    }
    return instance;
  }

  private AppExecutors() {

  }

  // m in mPrivate is myPrivate
  private final ScheduledExecutorService mNetworkIO = Executors.newScheduledThreadPool(3);
  public ScheduledExecutorService networkIO() {
    return mNetworkIO;
  }
}
