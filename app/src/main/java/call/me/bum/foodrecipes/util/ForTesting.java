package call.me.bum.foodrecipes.util;

import android.util.Log;
import call.me.bum.foodrecipes.model.Recipe;
import java.util.List;

public class ForTesting {
  public static void printRecipes(List<Recipe> list, String tag) {
    for (Recipe recipe : list){
      Log.d(tag, "printRecipes: " + recipe.getImage_url());
    }
  }
}
