package call.me.bum.foodrecipes.util;

public class Constants {

    public static final String BASE_URL = "https://www.food2fork.com";
    public static final String API_KEY = "08313c0a26cc84b73ea79eb035f43cee";
    public static final int NETWORK_TIMEOUT = 3000; // 3000 millis

    public static final String[] DEFAULT_SEARCH_CATEGORIES =
            {"Barbeque", "Breakfast", "Chicken", "Beef", "Brunch", "Dinner", "Wine", "Italian"};

    public static final String[] DEFAULT_SEARCH_CATEGORY_IMAGES =
            {
                    "barbeque",
                    "breakfast",
                    "chicken",
                    "beef",
                    "brunch",
                    "dinner",
                    "wine",
                    "italian"
            };
}
