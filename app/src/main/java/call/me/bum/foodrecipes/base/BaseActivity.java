package call.me.bum.foodrecipes.base;

import android.app.Activity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import call.me.bum.foodrecipes.R;

public abstract class BaseActivity extends AppCompatActivity implements IProgressBar {

  private ProgressBar mProgressBar;

  @Override
  public void setContentView(int layoutResID) {
    ConstraintLayout constraintLayout = (ConstraintLayout) getLayoutInflater().inflate(R.layout.activity_base, null);
    FrameLayout frameLayout = constraintLayout.findViewById(R.id.activity_content);
    mProgressBar = constraintLayout.findViewById(R.id.progress_bar);

    // Set the layoutRes to Framelayout content
    getLayoutInflater().inflate(layoutResID, frameLayout, true);
    super.setContentView(constraintLayout);
  }

  @Override
  public void showProgressBar(boolean visibility) {
    mProgressBar.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
  }

  public ProgressBar getProgressBar() {
    return mProgressBar;
  }
}
