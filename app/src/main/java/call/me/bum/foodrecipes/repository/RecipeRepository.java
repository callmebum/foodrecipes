package call.me.bum.foodrecipes.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import call.me.bum.foodrecipes.model.Recipe;
import call.me.bum.foodrecipes.request.RecipeApiClient;
import java.util.List;

public class RecipeRepository {

  //Network Api Client
  private RecipeApiClient mRecipeApiClient;

  // Singleton
  private static RecipeRepository instance;

  // vars
  private String mQuery;
  private int mPageNumber;
  public static RecipeRepository getInstance() {
    if (instance == null) {
      instance = new RecipeRepository();
    }
    return instance;
  }

  private RecipeRepository() {
    mRecipeApiClient = RecipeApiClient.getInstance();
  }

  public LiveData<List<Recipe>> getRecipes() {
    return mRecipeApiClient.getRecipes();
  }

  public void searchRecipes(String query, int pageNumber) {
    if (pageNumber == 0) {
      pageNumber = 1;
    }
    mQuery = query;
    mPageNumber = pageNumber;
    mRecipeApiClient.searchRecipesApi(query, pageNumber);
  }

  public void searchNextPage() {
    searchRecipes(mQuery, mPageNumber + 1);
  }

  public void cancelRequest() {
    mRecipeApiClient.cancelRequest();
  }
}
