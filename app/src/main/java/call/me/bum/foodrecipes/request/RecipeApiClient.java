package call.me.bum.foodrecipes.request;

import static call.me.bum.foodrecipes.util.Constants.NETWORK_TIMEOUT;

import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import call.me.bum.foodrecipes.background.AppExecutors;
import call.me.bum.foodrecipes.model.Recipe;
import call.me.bum.foodrecipes.request.response.RecipeResponse;
import call.me.bum.foodrecipes.request.response.RecipeSearchResponse;
import call.me.bum.foodrecipes.util.Constants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import retrofit2.Call;
import retrofit2.Response;

public class RecipeApiClient {


  private RetrieveRecipesRunnable mRetrieveRecipesRunnable;

  // data
  private MutableLiveData<List<Recipe>> mRecipes;

  // Singleton
  private static RecipeApiClient instance;

  public static RecipeApiClient getInstance() {
    if (instance == null) {
      instance = new RecipeApiClient();
    }
    return instance;
  }

  private RecipeApiClient() {
    mRecipes = new MutableLiveData<>();
  }

  public LiveData<List<Recipe>> getRecipes() {
    return mRecipes;
  }

  public void searchRecipesApi(String query, int pageNumber) {
    // ??? another call can be the same at here is that about singleton do
    if(mRetrieveRecipesRunnable != null) {
      mRetrieveRecipesRunnable = null;
    }
    mRetrieveRecipesRunnable = new RetrieveRecipesRunnable(query, pageNumber);
    final Future handler = AppExecutors.getInstance().networkIO().submit(mRetrieveRecipesRunnable);
    AppExecutors.getInstance().networkIO().schedule(() -> {
      // let the user know it timed out
      handler.cancel(true);
    }, NETWORK_TIMEOUT, TimeUnit.MILLISECONDS);
  }

  private class RetrieveRecipesRunnable implements Runnable {

    private static final String TAG = "RetrieveRecipesRunnable";

    // vars
    private String mQuery;
    private int mPageNumber;
    private boolean cancelRequest;

    public RetrieveRecipesRunnable(String query, int pageNumber) {
      this.mQuery = query;
      this.mPageNumber = pageNumber;
      this.cancelRequest = false;
    }

    @Override
    public void run() {
      try {
        Response response = getRecipes(mQuery, mPageNumber).execute();
        if (cancelRequest) {
          return;
        } else {
          if (response.code() == 200) {
            List<Recipe> list = new ArrayList<>(
                ((RecipeSearchResponse) response.body()).getRecipes());
            if (mPageNumber == 1) {
              mRecipes.postValue(list);
            } else {
              List<Recipe> currentRecipes = mRecipes.getValue();
              currentRecipes.addAll(list);
              mRecipes.postValue(currentRecipes);
            }
          } else {
            String error = response.errorBody().toString();
            Log.e(TAG, "run: " + error);
            mRecipes.postValue(null);
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
        mRecipes.postValue(null);
      }
    }

    private Call<RecipeSearchResponse> getRecipes(String query, int pageNumber) {
      return ApiConnect.getRecipeApi().searchRecipe(
          Constants.API_KEY,
          query,
          String.valueOf(pageNumber)
      );
    }

    private void cancelRequest() {
      Log.d(TAG, "cancelRequest: canceling search request");
      cancelRequest = true;
    }
  }

  public void cancelRequest () {
    if (mRetrieveRecipesRunnable != null) {
      mRetrieveRecipesRunnable.cancelRequest();
    }
  }
}
