package call.me.bum.foodrecipes.adapter;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import call.me.bum.foodrecipes.R;
import call.me.bum.foodrecipes.adapter.viewholder.CategoryViewHolder;
import call.me.bum.foodrecipes.adapter.viewholder.LoadingViewHolder;
import call.me.bum.foodrecipes.adapter.viewholder.RecipeViewHolder;
import call.me.bum.foodrecipes.model.Recipe;
import call.me.bum.foodrecipes.util.Constants;

public class RecipeRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int RECIPE_TYPE = 1;
    private static final int LOADING_TYPE = 2;
    private static final int CATEGORY_TYPE = 3;

    private List<Recipe> mRecipes;
    private OnRecipeListener onRecipeListener;

    public RecipeRecyclerAdapter(OnRecipeListener onRecipeListener) {
        this.onRecipeListener = onRecipeListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case LOADING_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_list_item, parent, false);
                return new LoadingViewHolder(view);
            case CATEGORY_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_category_list_item, parent, false);
                return new CategoryViewHolder(view, onRecipeListener);
            case RECIPE_TYPE:
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_recipe_list_item, parent, false);
                return new RecipeViewHolder(view, onRecipeListener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        int itemViewType = getItemViewType(position);
        if (mRecipes == null) {
            return;
        }

        // set the image
        RequestOptions requestOptions = new RequestOptions()
                .centerCrop()
                .error(R.drawable.ic_launcher_background);

        switch (itemViewType) {
            case RECIPE_TYPE: {
                RecipeViewHolder recipeViewHolder = (RecipeViewHolder) holder;

                Glide.with(recipeViewHolder.itemView.getContext())
                        .setDefaultRequestOptions(requestOptions)
                        .load(mRecipes.get(position).getImage_url())
                        .into(recipeViewHolder.image);
                recipeViewHolder.title.setText(mRecipes.get(position).getTitle());
                recipeViewHolder.publisher.setText(mRecipes.get(position).getPublisher());
                recipeViewHolder.socialScore.setText(String.valueOf(Math.round(mRecipes.get(position).getSocial_rank())));
                return;
            }
            case CATEGORY_TYPE: {
                CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;


                Uri path = Uri.parse("android.resource://call.me.bum.foodrecipes/drawable/" + mRecipes.get(position).getImage_url());
                Glide.with(categoryViewHolder.itemView.getContext())
                        .setDefaultRequestOptions(requestOptions)
                        .load(path)
                        .into(categoryViewHolder.categoryImage);
                categoryViewHolder.categoryTitle.setText(mRecipes.get(position).getTitle());

            }

        }
    }

    @Override
    public int getItemCount() {
        if (mRecipes == null)
            return 0;
        return mRecipes.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mRecipes.get(position).getSocial_rank() == -1) {
            return CATEGORY_TYPE;
        } else if (mRecipes.get(position).getTitle().equals("Loading...")) {
            return LOADING_TYPE;
        } else if (position == mRecipes.size() - 1
                    && position != 0
                    && !mRecipes.get(position).getTitle().equals("EXHAUSTED...")) {
            return LOADING_TYPE;
        } else {
            return RECIPE_TYPE;
        }
    }

    public void displayLoading() {
        if (!isLoading()) {
            Recipe recipe = new Recipe();
            recipe.setTitle("Loading...");
            List<Recipe> loadingList = new ArrayList<>();
            loadingList.add(recipe);
            mRecipes = loadingList;
        }
    }

    private boolean isLoading() {
        if (mRecipes != null) {
            if (mRecipes.size() > 0) {
                if (mRecipes.get(mRecipes.size() - 1).getTitle().equals("Loading..."))
                    return true;
            }
        }
        return false;
    }

    public void displaySearchCategory() {
        List<Recipe> categories = new ArrayList<>();
        for (int i = 0; i < Constants.DEFAULT_SEARCH_CATEGORIES.length; i++) {
            Recipe recipe = new Recipe();
            recipe.setTitle(Constants.DEFAULT_SEARCH_CATEGORIES[i]);
            recipe.setImage_url(Constants.DEFAULT_SEARCH_CATEGORY_IMAGES[i]);
            recipe.setSocial_rank(-1);
            categories.add(recipe);
        }
        mRecipes = categories;
        notifyDataSetChanged();
    }

    public void setRecipes(List<Recipe> recipes) {
        this.mRecipes = recipes;
        notifyDataSetChanged();

    }

    public Recipe getSelectedRecipe (int position) {
        if (mRecipes != null && mRecipes.size() > 0) {
            return mRecipes.get(position);
        }
        return null;
    }
}
