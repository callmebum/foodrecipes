package call.me.bum.foodrecipes.base;

public interface IProgressBar {
  void showProgressBar(boolean visibility);
}
